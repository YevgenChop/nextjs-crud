module.exports = {
    env: {
        domainURL: 'http://localhost:3000',
        jsonApiURL: 'https://jsonplaceholder.typicode.com',
        defaultHeaders: {
            'Content-type': 'application/json; charset=UTF-8',
          }
    },
  }