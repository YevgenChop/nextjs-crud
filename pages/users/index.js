import { useEffect, useState } from 'react';
import { signOut, useSession } from 'next-auth/client';
import Router from 'next/router';
import { Header, Layout, Table, Pagination } from '../../components/users';
import userService from '../../lib/users';

function Landing({ users, meta }) {
  const [session, loading] = useSession();
  if (loading) {
    return <p>Loading...</p>;
  }

  useEffect(() => {
    if (!session) {
      Router.replace('/');
    }
  }, [session]);

  const [usersData, setUsers] = useState(users);

  return (
    <>
      {session && (
        <Layout>
          <Header str='Users' signOut={signOut} />
          <Table users={usersData} />
          <Pagination meta={meta} setUsers={setUsers} />
        </Layout>
      )}
    </>
  );
}

export default Landing;

export async function getServerSideProps() {
  return userService.getUsers();
}
