import { signOut, useSession } from 'next-auth/client';
import { useEffect } from 'react';
import Router from 'next/router';

import { Header, Layout, Form } from '../../../components/users';
import userService from '../../../lib/users';

function Landing({ user }) {
  const [session, loading] = useSession();
  if (loading) {
    return <p>Loading...</p>;
  }

  useEffect(() => {
    if (!session) {
      Router.replace('/');
    }
  }, [session]);

  return (
    <Layout>
      <Header str='User' signOut={signOut}  />
      <Form user={user} />
    </Layout>
  );
}

export default Landing;

export async function getServerSideProps({ query }) {
  return userService.getUser(query);
}
