import { signOut, useSession } from 'next-auth/client';
import { useEffect } from 'react';
import Router from 'next/router';

import { Header, Layout, List } from '../../../components/posts';
import userService from '../../../lib/users';

function Posts({ posts }) {
  const [session, loading] = useSession();
  if (loading) {
    return <p>Loading...</p>;
  }

  useEffect(() => {
    if (!session) {
      Router.replace('/');
    }
  }, [session]);

  return (
    <Layout>
      <Header signOut={signOut} />
      <List posts={posts} />
    </Layout>
  );
}

export async function getServerSideProps({ query }) {
  return userService.getPosts(query);
}

export default Posts;
