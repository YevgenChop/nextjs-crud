import Head from 'next/head'
import Link from "next/link"
import { signIn, signOut, useSession } from "next-auth/client"
import styles from '../styles/Home.module.css'

export default function Home() {
  const [session, loading] = useSession()
  if (loading) {
    return <p>Loading...</p>
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        {!session && (
          <>
            Not signed in <br />
            <button className="btn btn__primary btn__icon" onClick={signIn}>Sign in</button>
          </>
        )}
        {session && (
          <>
            Signed in as {session.user.name} <br />
            <button className="btn btn__primary btn__icon" onClick={signOut}>Sign out</button>
          </>
        )}

        { session && <div style={{}}>
          <Link href="/users">
            <button className="btn btn__cancel btn__icon">Go to users page</button>
          </Link>
        </div>}
      </main>

      <footer className={styles.footer}>

      </footer>
    </div>
  )
}
