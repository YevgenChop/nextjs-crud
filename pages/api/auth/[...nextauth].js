import NextAuth from "next-auth"
import Providers from "next-auth/providers"

const user = {
  name: 'admin',
  password: 'admin'
}

const isCorrectCredentials = credentials =>
  credentials.username === user.name &&
  credentials.password === user.password

const options = {
  providers: [
    Providers.Credentials({
      name: "Credentials",
      credentials: {
        username: { label: "Username", type: "text", placeholder: "username" },
        password: { label: "Password", type: "password" },
      },
      authorize: async credentials => {
        if (isCorrectCredentials(credentials)) {
          const user = { id: 1, name: "Super User" }
          return Promise.resolve(user)
        } else {
          return Promise.resolve(null)
        }
      },
    }),
  ],
}

export default (req, res) => NextAuth(req, res, options)