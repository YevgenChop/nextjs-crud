export const apiURL = `${process.env.jsonApiURL}/users`;

const pageCount = 4;

export default async function handler(req, res) {
  const {
    query: { id },
    body,
    method,
  } = req;

  switch (method) {
    case 'GET':
      try {
        const result = await fetch(`${apiURL}`);
        const users = await result.json();

        return res.status(200).json({
          data: users.slice(0, pageCount),
          meta: {
            left: users.length - pageCount,
            page: 1,
            count: pageCount,
          },
        });
      } catch (error) {
        return res.status(404).json({});
      }
    case 'POST':
      try {
        const result = await fetch(`${apiURL}`);
        const users = await result.json();
        const { page } = JSON.parse(body);
        const currentPage = page ?? 1;
        const total = users.length;
        const from = (currentPage - 1) * pageCount;
        const left = total - (from + pageCount);

        return res.status(200).json({
          data: users.splice(from, pageCount),
          meta: {
            left: left >= 0 ? left : 0,
            page: currentPage,
            count: pageCount,
          },
        });
      } catch (error) {
        return res.status(404).json({});
      }
    default:
      res.setHeaders('Allow', ['GET', 'PUT', 'DELETE']);
      return res.status(405).json({}).end(`Method ${method} Not Allowed`);
  }
}
