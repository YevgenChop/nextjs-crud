const apiURL = `${process.env.jsonApiURL}/posts`;

export default async function handler(req, res) {
  const {
    query: { id },
    method,
    body,
  } = req;

  switch (method) {
    case 'GET':
      try {
        const result = await fetch(`${apiURL}?userId=${id}`);
        const posts = await result.json();

        return res.status(200).json({
          data: posts
        });
      } catch (error) {
        return res.status(404).json({
        });
      }
    default:
    	res.setHeaders("Allow", ["GET"]);
    	return res
    		.status(405)
    		.json({ success: false })
    		.end(`Method ${method} Not Allowed`);
  }

}
