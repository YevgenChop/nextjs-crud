import {headers} from '../index'

const apiURL = `${process.env.jsonApiURL}/posts`;

export default async function handler(req, res) {
  const {
    query: { id, postId },
    method,
    body,
  } = req;

  switch (method) {
    case 'PUT':
      try {
        const result = await fetch(`${apiURL}/${postId}`, {
          method,
          body,
          headers,
        });
        const post = await result.json();

        return res.status(200).json({
          data: post,
        });
      } catch (error) {
        return res.status(400).json({message: "something wrong!"});
      }
    case "DELETE":
    	try {
        const result = await fetch(`${apiURL}/${id}`, {
          method
        });
    		return res.status(200).json({
    			data: { id },
    		});
    	} catch (error) {
    		return res.status(400).json({message: "something wrong!"});
    	}
    default:
    	res.setHeaders("Allow", ["PUT", "DELETE"]);
    	return res
    		.status(405)
    		.json({ success: false })
    		.end(`Method ${method} Not Allowed`);
  }

}
