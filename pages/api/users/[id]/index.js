import { apiURL } from '../index';
export const headers = process.env.defaultHeaders;

export default async function handler(req, res) {
  const {
    query: { id },
    method,
    body,
  } = req;

  switch (method) {
    case 'GET':
      try {
        const result = await fetch(`${apiURL}/${id}`);
        const user = await result.json();

        return res.status(200).json({
          data: user,
        });
      } catch (error) {
        return res.status(400).json({});
      }
    case 'PUT':
      try {
        const result = await fetch(`${apiURL}/${id}`, {
          method,
          body,
          headers,
        });
        const user = await result.json();

        return res.status(200).json({
          data: user,
        });
      } catch (error) {
        return res.status(400).json({});
      }
    case 'DELETE':
      try {
        await fetch(`${apiURL}/${id}`, { method });

        return res.status(200).json({
          data: { id },
        });
      } catch (error) {
        return res.status(400).json({});
      }
    default:
      res.setHeaders('Allow', ['GET', 'PUT', 'DELETE']);
      return res.status(405).json({}).end(`Method ${method} Not Allowed`);
  }
}
