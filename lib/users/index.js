const baseUrl = process.env.domainURL + '/api/users';

const getUser = async (query) => {
  const id = query.id;

  const res = await fetch(`${baseUrl}/${id}`);
  const { data } = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      user: data,
    },
  };
};

const getUsers = async () => {

  const res = await fetch(`${baseUrl}`, {method: 'GET'});
  const {data, meta} = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      users: data,
      meta,
    },
  };
};

const updateUser = async (query, bodyData) => {
  const id = query.id;

  const res = await fetch(`${baseUrl}/${id}`, {
    method: 'PUT',
    body: JSON.stringify(bodyData),
  });
  const { data } = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      user: data,
    },
  };
};

const deleteUser = async (query) => {
  const id = query.id;

  const res = await fetch(`${baseUrl}/${id}`, { method: 'DELETE' });
  const { data } = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      id: data.id,
      status: 'deleted',
    },
  };
};

const getPosts = async (query) => {
  const id = query.id;

  const res = await fetch(`${baseUrl}/${id}/posts`);
  const { data } = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      posts: data,
    },
  };
};

const deleteOnePost = async (query) => {
  const id = query.id;
  const userId = query.userId;

  const res = await fetch(`${baseUrl}/${userId}/posts/${id}`, { method: 'DELETE' });
  const { data } = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      id: data.id,
    },
  };
};

const updateOnePost = async (query, bodyData) => {
  const id = query.id;
  const userId = query.userId;

  const res = await fetch(`${baseUrl}/${userId}/posts/${id}`, {
	   method: 'PUT',
	   body: JSON.stringify({userId, ...bodyData})
	});
  const { data } = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      post: data,
    },
  };
};

const  filterUsers= async (bodyData) => {
  const res = await fetch(`${baseUrl}`, {
	   method: 'POST',
	   body: JSON.stringify(bodyData)
	});
  const { data, meta } = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      users: data,
      meta,
    },
  };
};

export default {
  getUsers,
  updateUser,
  getUser,
  deleteUser,
  getPosts,
  deleteOnePost,
  updateOnePost,
  filterUsers,
};
