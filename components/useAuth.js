import { useEffect } from 'react';
import Router from 'next/router';
import { useSession } from 'next-auth/client';

function useAuth() {
    const [session, loading] = useSession();

    useEffect(() => {
      if(!session){
        Router.replace("/")
      }
    }, [session])

  }

  export default useAuth;