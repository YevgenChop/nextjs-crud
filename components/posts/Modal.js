import { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { useForm } from 'react-hook-form';

import { CheckSVG, CloseSVG } from '../../icons';
import styles from './styles/form.module.sass';

export function Modal({ isOpen, post, onClose, onSubmit }) {
  const { register, handleSubmit, setValue, reset } = useForm();

  const state = post;

  const closeModal = () => {
    reset();
    onClose();
  };

  const onSubmitHandler = async (data) => {
    await onSubmit(post)(data);
    onClose();
  };

  useEffect(() => {
    setValue('title', state.title);
    setValue('body', state.body);
  }, [state, setValue]);

  return isOpen
    ? ReactDOM.createPortal(
        <div className='modal'>
          <div className='modal__content'>
            <header className='header modal__header'>
              <h1 className='header__h2'>Edit Post</h1>
              <button className='btn btn__compact btn__close' onClick={closeModal}>
                <CloseSVG />
              </button>
            </header>

            <form className={`${styles.form} modal__form`} onSubmit={handleSubmit(onSubmitHandler)} noValidate>
              <div className={styles.form__element}>
                <label htmlFor='titleInput' className={styles.label}>
                  Title
                </label>
                <input type='text' id='titleInput' name='title' placeholder='Title' className='input' ref={register} />
              </div>
              <div className={styles.form__element}>
                <label htmlFor='bodyInput' className={styles.label}>
                  Message
                </label>
                <textarea
                  type='text'
                  id='bodyInput'
                  name='body'
                  placeholder='Message'
                  className='input'
                  rows='5'
                  ref={register}
                />
              </div>

              <div className={styles.form__action}>
                <button className='btn btn__icon btn__cancel' type='button' onClick={closeModal}>
                  <CloseSVG /> Cancel
                </button>
                <button className='btn btn__primary btn__icon' type='submit'>
                  <CheckSVG /> {state.selectedEmployee ? 'Update' : 'Submit'}
                </button>
              </div>
            </form>
          </div>
        </div>,
        document.body
      )
    : null;
}
