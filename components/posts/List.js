import { useState } from 'react';
import { PencilSVG, TrashSVG } from '../../icons';
import styles from './styles/list.module.sass';
import userService from '../../lib/users';
import { Modal } from './Modal';

export function List({ posts }) {
  const [postList, setList] = useState(posts);
  const [selectedPost, setPost] = useState({});
  const [open, setOpen] = useState(false);

  const deleteHandler = (data) => async () => {
    const res = await userService.deleteOnePost(data);
    const newList = postList.filter((el) => el.id !== data.id);
    setList(newList);
  };

  const editHandler = (id) => () => {
    setPost(postList.filter((el) => el.id === id)[0]);
    setOpen(true);
  };

  const onSubmit = (post) => async (data) => {
    const { props } = await userService.updateOnePost(post, data);
    const newList = postList.map((el) => {
      if (el.id === props.post.id) return props.post;

      return el;
    });
    setList(newList);
  };

  const onClose = () => setOpen(false);

  return (
    <>
      <ul>
        {postList.map(({ id, title, body, userId }) => {
          return (
            <li key={id} className={styles.msgWrapper}>
              <div className={styles.msg}>
                <header className={styles.header}>
                  <h3 className={styles.title}>{title}</h3>
                  <div className={styles.action}>
                    <button className='btn btn__edit btn__compact' onClick={editHandler(id)}>
                      <PencilSVG />
                    </button>
                    <button className='btn btn__delete btn__compact' onClick={deleteHandler({ id, userId })}>
                      <TrashSVG />
                    </button>
                  </div>
                </header>
                <div className={styles.content}>
                  <p>{body}</p>
                </div>
              </div>
            </li>
          );
        })}
      </ul>

      {open && <Modal isOpen={open} post={selectedPost} onClose={onClose} onSubmit={onSubmit} />}
    </>
  );
}
