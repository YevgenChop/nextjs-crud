import Head from 'next/head';
import styles from './styles/layout.module.sass';

export function Layout({ children }) {
  return (
    <main className={styles.layout}>
      <Head>
        <title>Posts</title>
      </Head>
      {children}
    </main>
  );
}
