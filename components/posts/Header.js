import { useRouter } from 'next/router';

export function Header({signOut}) {
	const router = useRouter();
	const cancelHandler = () => router.push(`/users`);
	return (
		<header className="header">
			<h1 className="header__h1">
				<span>Message List</span>
			</h1>
			<button
				className="btn btn__primary btn__icon"
				onClick={cancelHandler}
			>
				 <span>Back to users</span>
			</button>
			<button
				className="btn btn__cancel btn__icon"
				onClick={signOut}
			>
				<span>Log out</span>
			</button>
		</header>
	);
}
