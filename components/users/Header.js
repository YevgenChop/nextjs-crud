export function Header({str, signOut}) {

	return (
		<header className="header">
			<h1 className="header__h1">
				<span>{str}</span> page
			</h1>
			<button
				className="btn btn__cancel btn__icon"
				onClick={signOut}
			>
				<span>Log out</span>
			</button>
		</header>
	);
}
