
import { useRouter } from 'next/router';

export function Table({ users }) {
  const router = useRouter();
  const redirectHandler = (id) => () => {
    router.push(`/users/${id}`);
  };

  return (
    <table className='table'>
      <thead className='table__head'>
        <tr>
          <th>Name</th>
          <th>User name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Website</th>
        </tr>
      </thead>

      <tbody className='table__body'>
        {users.map(({ id, name, username, email, phone, website }) => (
          <tr onClick={redirectHandler(id)} key={id}>
            <td>{name}</td>
            <td>{username}</td>
            <td>{email}</td>
            <td>{phone}</td>
            <td>{website}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
