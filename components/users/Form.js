import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router';

import styles from './styles/form.module.sass';
import userService from '../../lib/users';

export function Form({ user }) {
  const router = useRouter();
  const [status, setStatus] = useState('edit');
  const { register, handleSubmit, setValue } = useForm();
  let userData = user;

  const onSubmit = async (data) => {
    const result = await userService.updateUser(user, data);
    userData = result
  };

  const cancelHandler = () => router.push(`/users`);
  const messageHandler = () => router.push(`/users/${user.id}/posts`);

  const deleteHandler = async () => {
    const res = await userService.deleteUser(user)
    setStatus(res.props.status)
  };

  useEffect(() => {
    setValue('name', userData.name);
    setValue('username', userData.username);
    setValue('email', userData.email);
    setValue('phone', userData.phone);
    setValue('website', userData.website);
  }, [userData, setValue]);

  return <div className={styles.formWrapper}>
	  {status !== 'deleted' ? (
		<form className={styles.form} onSubmit={handleSubmit(onSubmit)} noValidate>
			<div className={styles.form__element}>
				<label htmlFor='nameInput' className={styles.label}>
				Full name
				</label>
				<input
					type='text'
					id='nameInput'
					name='name'
					placeholder='Full name'
					className='input'
					ref={register} />
			</div>
			<div className={styles.form__element}>
				<label htmlFor='usernameInput' className={styles.label}>
				Username
				</label>
				<input
					type='text'
					id='usernameInput'
					name='username'
					placeholder='Username'
					className='input'
					ref={register} />
			</div>
			<div className={styles.form__element}>
				<label htmlFor='emailInput' className={styles.label}>
				Email
				</label>
				<input
					type='email'
					id='emailInput'
					name='email'
					placeholder='Email'
					className='input'
					ref={register} />
			</div>
			<div className={styles.form__element}>
				<label htmlFor='phoneInput' className={styles.label}>
				Phone
				</label>
				<input
					type='text'
					id='phoneInput'
					name='phone'
					placeholder='Phone'
					className='input'
					ref={register} />
			</div>
			<div className={styles.form__element}>
				<label htmlFor='webInput' className={styles.label}>
				Website
				</label>
				<input
					type='text'
					id='webInput'
					name='website'
					placeholder='Website'
					className='input'
					ref={register} />
			</div>
			<div className={styles.form__action}>
				<button
					className='btn btn__icon btn__cancel'
					type='button'
					onClick={cancelHandler}>
				Go back
				</button>
				<button
					className='btn'
					type='button'
					onClick={messageHandler}>
				Show Messages
				</button>
				<button
					className='btn btn__delete'
					type='button'
					onClick={deleteHandler}>
				Delete
				</button>
				<button
					className='btn btn__primary btn__icon'
					type='submit'>
				Update
				</button>
			</div>
		</form>
	) : (
		<div className={styles.info}>
          <span>User deleted successfully!</span>
          <button className='btn btn__icon btn__cancel' type='button' onClick={cancelHandler}>
            Users List
          </button>
        </div>
	)}
  </div>
}
