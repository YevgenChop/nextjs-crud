import { useState } from 'react';
import userService from '../../lib/users';

export function Pagination({ setUsers, meta }) {
  const [filter, setFilter] = useState(meta);

  const clickHandler = (data) => async () => {
    const { props } = await userService.filterUsers(data);
    setUsers(props.users);
    setFilter(props.meta);
  };

  return (
    <div className='paginationWrapper'>
      <div className='pagination'>
        <div className='text'>Left {filter.left}</div>
        <div className='text'>Page {filter.page}</div>
        <button
          className='btn btn__compact btn__margin'
          onClick={clickHandler({ page: filter.page - 1 })}
          disabled={filter.page === 1}
        >
          prev page
        </button>
        <button
          className='btn btn__compact btn__margin'
          onClick={clickHandler({ page: filter.page + 1 })}
          disabled={filter.left < 1}
        >
          next page
        </button>
      </div>
    </div>
  );
}
